class AddCategoryIdToMeals < ActiveRecord::Migration[5.2]
  def change
    add_column :meals, :category_id, :integer
  end
end
