class AddPositionToProcedures < ActiveRecord::Migration[5.2]
  def change
    add_column :procedures, :position, :integer
  end
end
