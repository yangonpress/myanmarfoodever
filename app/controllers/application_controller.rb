# frozen_string_literal: true

class ApplicationController < ActionController::Base
		include Pagy::Backend
		
	  before_action :configure_permitted_parameter, if: :devise_controller?

	  protected
	  def configure_permitted_parameter
	  	devise_parameter_sanitizer.permit(:sign_up){|u| u.permit(:first_name, :last_name, :user_name,:about, :birthday, :email, :password, :remember_me)}
	  	devise_parameter_sanitizer.permit(:sigin_in){|u| u.permit(:email, :password, :remember_me)}
	  	devise_parameter_sanitizer.permit(:account_update){|u| u.permit(:first_name, :last_name, :user_name,:about, :birthday, :email, :password,:current_password, :remember_me)}

	  end
end
