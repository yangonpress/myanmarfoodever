# frozen_string_literal: true

class MealsController < ApplicationController
  before_action :set_meal, only: [:show, :edit, :update, :destroy, :upvote]

  # GET /meals
  # GET /meals.json
  def index
    if params[:category].blank?
    @pagy, @meals = pagy(Meal.all)
    else
      @category_id = Category.find_by(name: params[:category]).id
      @meals = Meal.where(:category_id => @category_id)
    end
    if params[:search]
      @search_term = params[:search]
      @meals = @meals.search_by(@search_term)
    end
  end


  # GET /meals/1
  # GET /meals/1.json
  def show
    @comments = @meal.comments.all
    @comment = @meal.comments.build
  end

  # GET /meals/new
  def new
   @meal = current_user.meals.build
   @categories = Category.all.map{ |c| [c.name, c.id] }
  end

  # GET /meals/1/edit
  def edit
    @categories= Category.all.map{ |c| [c.name, c.id] }
  end

  # POST /meals
  # POST /meals.json
  def create
    @meal = current_user.meals.build(meal_params)
    @meal.category_id = params[:category_id]
    respond_to do |format|
      if @meal.save
        format.html { redirect_to @meal, notice: 'Meal was successfully created.' }
        format.json { render :show, status: :created, location: @meal }
      else
        format.html { render :new }
        format.json { render json: @meal.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /meals/1
  # PATCH/PUT /meals/1.json
  def update
    @meal.category_id = params[:category_id]
    respond_to do |format|
      if @meal.update(meal_params)
        format.html { redirect_to @meal, notice: 'Meal was successfully updated.' }
        format.json { render :show, status: :ok, location: @meal }
      else
        format.html { render :edit }
        format.json { render json: @meal.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /meals/1
  # DELETE /meals/1.json
  def destroy
    @meal.destroy
    respond_to do |format|
      format.html { redirect_to meals_url, notice: 'Meal was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def upvote
    @meal.upvote_by current_user
    redirect_to meal_path(@meal)
  end
  private

  # Use callbacks to share common setup or constraints between actions.
  def set_meal
    @meal = Meal.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def meal_params
    params.require(:meal).permit(:name, :description, :picture, :category_id, ingredients_attributes: [:id, :name, :_destroy], directions_attributes: [:id, :name, :_destroy])
  end
end
