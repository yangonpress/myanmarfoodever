class CommentsController < ApplicationController

  def create
    @comment = current_user.comments.build
    @meal = Meal.find(params[:meal_id])
    @comment = @meal.comments.create(params[:comment].permit(:name, :body))
    redirect_to meal_path(@meal)
  end
end
