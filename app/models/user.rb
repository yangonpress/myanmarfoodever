class User < ApplicationRecord
	has_many :meals
	has_many :comments
	has_many :ingredients
	has_many :procedures
	mount_uploader :picture, PictureUploader
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  include Gravtastic
	gravtastic :secure => true,
              :size => 500
end
