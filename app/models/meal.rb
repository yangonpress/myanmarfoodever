# frozen_string_literal: true

class Meal < ApplicationRecord
	acts_as_votable
	has_many :comments
	has_many :ingredients
	has_many :directions
	accepts_nested_attributes_for :ingredients,
																reject_if: proc { |attributes| attributes['name'].blank? },
																allow_destroy: true
	accepts_nested_attributes_for :directions,
																reject_if: proc { |attributes| attributes['name'].blank? },
																allow_destroy: true
	validates :name, :description, :picture, presence: true

	belongs_to :user
	belongs_to :category
	mount_uploader :picture, PictureUploader



	def self.search_by(search_term)
		where ("LOWER(name) LIKE :search_term"), search_term: "%#{search_term.downcase}"
	end
end
