class Comment < ApplicationRecord
  belongs_to :meal , optional: true
  belongs_to :user , optional: true
end
