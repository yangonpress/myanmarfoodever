 # frozen_string_literal: true

source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }
gem 'dotenv-rails'
ruby '2.3.3'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 5.2.1'
# Use sqlite3 as the database for Active Record
gem 'carrierwave'
gem 'sqlite3'
gem 'devise', '~> 4.6', '>= 4.6.2'
gem 'puma', '~> 3.11'
gem 'sass-rails', '~> 5.0'
gem 'uglifier', '>= 1.3.0'
gem 'duktape'
gem 'coffee-rails', '~> 4.2'
gem 'turbolinks', '~> 5'
gem 'jbuilder', '~> 2.5'
gem 'simple_form', '~> 4.1'
gem 'jquery-rails'
gem 'jquery-ui-rails'
gem 'acts_as_list'
gem 'tzinfo-data', platforms: %i[mingw mswin x64_mingw jruby]
gem 'bootstrap-sass', '~> 3.4.1'
gem 'bootsnap', '= 1.3.2', require: false
gem 'gravtastic', '~> 3.2', '>= 3.2.6'
gem 'cocoon', '~> 1.2', '>= 1.2.14'
gem 'pagy', '~> 3.7', '>= 3.7.2'
gem 'acts_as_votable', '~> 0.12.1'
group :development, :test do
  gem 'byebug', platforms: %i[mri mingw x64_mingw]
end

group :development do
  gem 'capistrano'
  gem 'capistrano-bundler'
  gem 'capistrano-rails'
  gem 'capistrano-rbenv'
  gem 'capistrano3-puma'
  gem 'rubocop'
  gem 'web-console', '>= 3.3.0'
end

group :test do
  gem 'capybara', '>= 2.15'
  gem 'selenium-webdriver'
  gem 'chromedriver-helper'
end
