# frozen_string_literal: true

Rails.application.routes.draw do
  get 'users/show'
  devise_for :users
	resources :meals do
    resources :comments
  end



  resources :ingredients do
    collection do
      patch :sort
    end
  end

  resources :meals do
    member do
      put "like", to: "meals#upvote"
    end
  end

  get 'pages/info'
  get 'pages/contact'
  get 'pages/faq'
  resources :comments
  root to: redirect('/meals')
  resources :users
end
